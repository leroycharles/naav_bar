# Naav_bar

## Overview

- [Type] Personnal Project
- [Objectives] Create a polyvalent bar managing app
- [Goal] Improve myself and show what I can do

## Objectives

### Phase 0 - Initialisation

- [ ] Define What, Why, How of the solution
- [ ] Confront with domain

### Phase 1 - Basics

- [ ] Create a working till system

### Phase 2 - Managing

- [ ] Create a stock managing system

### Phase 3 - Communication

- [ ] Create a community

### Phase 4 - Optimisation

- [ ] Create a better system

### Phase 5 - Prediction

- [ ] Create a prediction for a week, a month, a year
